﻿using System.Collections.Generic;

namespace SimplyLogin1.Common
{
    /// <summary>
    /// Contains all user role names used in the app
    /// </summary>
    public static class UserRole
    {
        public const string NewUser = "NewUser";
        public const string Member = "Member";
        public const string Moderator = "Moderator";
        public const string Admin = "Admin";
        public const string Moderation = "Moderator, Admin";
        public const string Memberhsip = "Member, Moderator, Admin";
        public static readonly IEnumerable<string> All = new[] { NewUser, Member, Moderator, Admin };
    }
}
