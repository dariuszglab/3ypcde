﻿namespace SimplyLogin1.Common
{
    /// <summary>
    /// Contins commonly used strings
    /// </summary>
    public static class Consts
    {
        public const string Home = "Home";
        public const string Account = "Account";
        public const string Meeting = "Meeting";
        public const string Resource = "Resource";
        public const string Brand = "Simply Login";
        public const string Contact = "Contact";
        public const string About = "About";
    }
}
