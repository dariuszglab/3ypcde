﻿namespace SimplyLogin1.Common
{
    /// <summary>
    /// Contains few helper methods for string manipulation
    /// </summary>
    public static class StringExtensions
    {
        public static bool IsEmpty(this string arg)
        {
            return string.IsNullOrEmpty(arg);
        }

        public static bool IsNotEmpty(this string arg)
        {
            return !IsEmpty(arg);
        }

        /// <summary>
        /// Converts string to int
        /// </summary>
        /// <param name="arg">string number to convert</param>
        /// <returns></returns>
        public static int Parse(this string arg)
        {
            return arg.IsEmpty() ? 0 : int.Parse(arg);
        }
    }
}
