﻿using AutoMapper;
using SimplyLogin1.Data.Entities;
using SimplyLogin1.Models;

namespace SimplyLogin1.Common
{
    /// <summary>
    /// Configures mapping between types for AutoMapper
    /// </summary>
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<User, RegisterViewModel>().ReverseMap();
            CreateMap<User, LoginViewModel>().ReverseMap();
            CreateMap<User, UserViewModel>().ReverseMap();

            CreateMap<Resource, ResourceViewModel>().ReverseMap();

            CreateMap<Meeting, MeetingViewModel>().ReverseMap();

            CreateMap<Message, ContactViewModel>().ReverseMap();
        }
    }
}
