﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SimplyLogin1.Data;
using SimplyLogin1.Data.Entities;
using SimplyLogin1.Services;
using System;

namespace SimplyLogin1
{
    public class Startup
    {
        private readonly IHostingEnvironment _environment;
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            _environment = environment;
            _configuration = configuration;
        }

        public IConfiguration Configuration => _configuration;

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DbCtx>(SetupDatabase(), ServiceLifetime.Scoped);
            services.AddIdentity<User, Role>(SetupIdentity)
                .AddEntityFrameworkStores<DbCtx>()
                .AddDefaultTokenProviders();
            services.ConfigureApplicationCookie(SetupCookie);
            services.AddAutoMapper();
            SetupCulture(services);
            services.AddMvc(SetupMvc);

            ///setup services
            if (_configuration["Settings:MailClient"] == "Console")
            {
                services.AddSingleton<IMailClient, ConsoleMailClient>();
            }
            else
            {
                services.AddSingleton<IMailClient, MailClient>();
            }
            services.AddSingleton<EmailService>();
            services.AddSingleton<IHostedService>(x => x.GetService<EmailService>());

            services.AddTransient<DbSeeder>();
            services.AddScoped<ResourceRepository>();
            services.AddScoped<MeetingRepository>();
            services.AddTransient<IUserService, UserService>();
        }


        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseMvc(SetupRoutes);

            SeedDatabase(app);
        }

        /// <summary>
        /// Enables HTTPS
        /// </summary>
        private void SetupMvc(MvcOptions options)
        {
            //enable https
            if (_environment.IsProduction())
            {
                options.Filters.Add(new RequireHttpsAttribute());
            }
        }

        /// <summary>
        /// Configures rules related to accounts security
        /// </summary>
        private void SetupIdentity(IdentityOptions options)
        {
            options.User.RequireUniqueEmail = false;

            options.Password.RequireDigit = false;
            options.Password.RequireNonAlphanumeric = false;
            options.Password.RequiredLength = 4;
            options.Password.RequireUppercase = false;
            options.Password.RequireLowercase = false;
            options.Password.RequiredUniqueChars = 1;

            options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
            options.Lockout.MaxFailedAccessAttempts = 15;
            options.Lockout.AllowedForNewUsers = false;
        }

        /// <summary>
        /// Configures basic things aboout cookies
        /// </summary>
        private void SetupCookie(CookieAuthenticationOptions options)
        {
            options.Cookie.HttpOnly = true;
            options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
            options.LoginPath = "/Account/Login";
            options.AccessDeniedPath = "/Account/AccessDenied";
            options.SlidingExpiration = true;
        }

        /// <summary>
        /// Configures general rules between urls and things they do
        /// </summary>
        private void SetupRoutes(IRouteBuilder routes)
        {
            routes.MapRoute(
                name: "default",
                template: "{controller=Home}/{action=Index}/{id?}");
        }

        /// <summary>
        /// Fills the databse with some initial data
        /// </summary>
        private async void SeedDatabase(IApplicationBuilder app)
        {
            if (!_environment.IsDevelopment()) return;

            using (var scope = app.ApplicationServices.CreateScope())
            {
                var seeder = scope.ServiceProvider.GetService<DbSeeder>();
                await seeder.Seed();
            }
        }

        /// <summary>
        /// Configures default culutre of requests, this is important for handling requests in different locales
        /// </summary>
        private static void SetupCulture(IServiceCollection services)
        {
            services.Configure<RequestLocalizationOptions>(x =>
            {
                x.DefaultRequestCulture = new RequestCulture("en-US");
            });
        }

        /// <summary>
        /// Configures database connection with use of Connection String read from configuration
        /// </summary>
        private Action<DbContextOptionsBuilder> SetupDatabase()
        {
            var connectionString = _configuration
                .GetConnectionString("SQLiteConnectionString");
            return x => x.UseSqlite(connectionString);
        }

    }
}
