﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SimplyLogin1.Common;
using SimplyLogin1.Data;
using SimplyLogin1.Data.Entities;
using SimplyLogin1.Models;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SimplyLogin1.Services
{
    /// <summary>
    /// Service used for all user, role and account realated tasks
    /// </summary>
    public class UserService : IUserService
    {
        private readonly RoleManager<Role> _roleManager;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IMapper _mapper;
        private readonly DbCtx _ctx;
        private readonly EmailService _emailService;

        public UserService(
            IMapper mapper,
            SignInManager<User> signInManager,
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            DbCtx ctx,
            EmailService emailService)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
            _ctx = ctx;
            _emailService = emailService;
        }

        /// <summary>
        /// Autheticates and logs the user into the system
        /// </summary>
        /// <param name="model">login form data</param>
        /// <returns>True if login succceded</returns>
        public async Task<bool> Login(LoginViewModel model)
        {
            var loginResult = await _signInManager.PasswordSignInAsync(
                model.Username, model.Password,
                model.RememberMe, false);

            return loginResult.Succeeded;
        }

        public async Task<bool> Login(RegisterViewModel model)
        {
            var loginResult = await _signInManager.PasswordSignInAsync(
                model.Username, model.Password,
                false, false);

            return loginResult.Succeeded;
        }


        /// <summary>
        /// Logs out currently logged in user
        /// </summary>
        public async Task Logout()
        {
            await _signInManager.SignOutAsync();
        }

        /// <summary>
        /// Adds new user into the system
        /// </summary>
        /// <param name="model">registration form data</param>
        /// <returns>True if user was added successfully</returns>
        public async Task<bool> Register(RegisterViewModel model)
        {
            var user = _mapper.Map<RegisterViewModel, User>(model);
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                var roleRes = await _userManager.AddToRoleAsync(user, UserRole.NewUser);
                return roleRes.Succeeded;
            }
            return false;
        }

        /// <summary>
        /// Returns currently logged in user
        /// </summary>
        /// <param name="principal">Controllers user</param>
        /// <returns>Logged in user entity</returns>
        public async Task<User> GetCurrentUser(ClaimsPrincipal principal)
        {
            if (principal == null) return null;

            var user = await _userManager.GetUserAsync(principal);

            return user;
        }

        /// <summary>
        /// Sets user role, removes all other roles
        /// </summary>
        /// <param name="id">user id</param>
        /// <param name="newRole">new role name</param>
        /// <returns>True if succeded</returns>
        public async Task<bool> SetRole(string id, string newRole)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return false;

            var oldRoles = await _userManager.GetRolesAsync(user);
            await _userManager.RemoveFromRolesAsync(user, oldRoles);
            var roleRes = await _userManager.AddToRoleAsync(user, newRole);

            if (roleRes.Succeeded)
            {
                var mapped = Map(user);
                mapped.Role = newRole;
                _emailService.SendUserRoleChanged(mapped);
            }
            return roleRes.Succeeded;
        }

        /// <summary>
        /// Deletes the user
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns>True if succeded</returns>
        public async Task<bool> Delete(string id)
        {
            if (id == null)
            {
                return false;
            }

            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return false;

            var meetingsToDelete = await _ctx.Meetings.
                Where(x => x.User.Id == id)
                .ToListAsync();

            _ctx.Meetings.RemoveRange(meetingsToDelete);

            var res = await _userManager.DeleteAsync(user);
            return res.Succeeded;
        }

        /// <summary>
        /// Returns all users
        /// </summary>
        /// <returns>Collection of all users</returns>
        public async Task<IEnumerable<UserViewModel>> GetAllUsers()
        {
            var users = _userManager.Users;
            var result = new List<UserViewModel>();
            foreach (var item in users)
            {
                var vm = _mapper.Map<User, UserViewModel>(item);
                //terrible brute force for now:
                //todo: include UserRole into User entity
                var roles = await _userManager.GetRolesAsync(item);
                vm.Role = roles.FirstOrDefault(); //.Aggregate((x, y) => $"{x}, {y}");
                result.Add(vm);
            }
            return result;
        }

        /// <summary>
        /// Returnts user with given id
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>User entity</returns>
        public async Task<User> Get(string id)
        {
            if (id == null)
            {
                return null;
            }
            return await _userManager.FindByIdAsync(id);
        }

        /// <summary>
        /// Maps user entity to form data model
        /// </summary>
        /// <param name="user">user to map</param>
        /// <returns>mapped form model</returns>
        public UserViewModel Map(User user)
        {
            var mapped = _mapper.Map<User, UserViewModel>(user);
            return mapped;
        }

        /// <summary>
        /// Changes user password
        /// </summary>
        /// <param name="user">user whom password is to be changed</param>
        /// <param name="currentPassword">current password of the user</param>
        /// <param name="newPassword">new password</param>
        /// <returns>True if succeded</returns>
        public async Task<bool> ChangePassword(User user, string currentPassword, string newPassword)
        {
            var res = await _userManager.ChangePasswordAsync(user, currentPassword, newPassword);
            return res.Succeeded;
        }

        /// <summary>
        /// Resets user password
        /// </summary>
        /// <param name="user">user whom password is to be reset</param>
        /// <param name="newPassword">new password to be set instead</param>
        /// <returns>True if succeded</returns>
        public async Task<bool> ResetPassword(User user, string newPassword)
        {
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var res = await _userManager.ResetPasswordAsync(user, token, newPassword);

            return res.Succeeded;
        }
    }
}
