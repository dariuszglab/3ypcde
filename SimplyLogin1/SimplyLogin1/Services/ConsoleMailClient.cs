﻿using System;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SimplyLogin1.Services
{
    /// <summary>
    /// A substitute class for mail sending, which shows mails in debug console instead of sending them
    /// </summary>
    public sealed class ConsoleMailClient : IMailClient
    {
        private readonly StringBuilder _sb;

        public ConsoleMailClient()
        { 
            _sb = new StringBuilder();
        }

        public Task Send(MailMessage message)
        {
            _sb.Append("### ");
            _sb.AppendLine(DateTime.Now.ToShortTimeString());
            _sb.Append("f:");
            _sb.AppendLine(message.From.Address);
            _sb.Append("t:");
            _sb.AppendLine(string.Join(", ", message.To));
            _sb.Append("s:");
            _sb.AppendLine(message.Subject);
            _sb.Append("b:");
            _sb.Append(message.Body);

            Console.WriteLine(_sb.ToString());
            _sb.Clear();
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _sb.Clear();
        }
    }
}
