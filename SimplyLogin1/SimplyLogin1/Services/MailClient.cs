﻿using Microsoft.Extensions.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SimplyLogin1.Services
{
    /// <summary>
    /// Real email client sending emails using credentials read from configuration.
    /// </summary>
    public sealed class MailClient : IMailClient
    {
        private readonly SmtpClient _smtpClient;

        public MailClient(IConfiguration configuration)
        {
            var host = configuration["EmailAccounts:Host"];
            var login = configuration["EmailAccounts:Email1"];
            var password = configuration["EmailAccounts:Password1"];

            _smtpClient = new SmtpClient(host)
            {
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(login, password)
            };
        }

        public async Task Send(MailMessage message)
        {
            await _smtpClient.SendMailAsync(message);
        }

        public void Dispose()
        {
            _smtpClient.Dispose();
        }
    }
}
