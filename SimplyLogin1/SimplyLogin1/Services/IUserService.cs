
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using SimplyLogin1.Data.Entities;
using SimplyLogin1.Models;

namespace SimplyLogin1.Services
{
    public interface IUserService
    {
        Task<bool> Login(LoginViewModel model);
        Task<bool> Login(RegisterViewModel model);
        Task Logout();
        Task<bool> Register(RegisterViewModel model);
        Task<User> GetCurrentUser(ClaimsPrincipal principal);
        Task<bool> SetRole(string id, string newRole);
        Task<bool> Delete(string id);
        Task<IEnumerable<UserViewModel>> GetAllUsers();
        UserViewModel Map(User user);
        Task<bool> ChangePassword(User user, string currentPassword, string newPassword);
        Task<bool> ResetPassword(User user, string newPassword);
    }
}