﻿using System;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SimplyLogin1.Services
{
    public interface IMailClient : IDisposable
    {
        /// <summary>
        /// Sends a mail message
        /// </summary>
        /// <param name="message">Mail to send</param>
        Task Send(MailMessage message);
    }
}