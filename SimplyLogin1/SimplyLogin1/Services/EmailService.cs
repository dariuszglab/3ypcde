﻿using Microsoft.Extensions.Configuration;
using SimplyLogin1.Common;
using SimplyLogin1.Models;
using System;
using System.Collections.Concurrent;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimplyLogin1.Services
{
    /// <summary>
    /// Class used for sending emails
    /// </summary>
    public class EmailService : BackgroundService
    {
        private readonly string FirstMail;
        private readonly string SecondMail;
        private const int MillisecondsDelay = 91337;

        private readonly ConcurrentQueue<MailMessage> _scheduledItems = new ConcurrentQueue<MailMessage>();
        private readonly Random _rand = new Random();
        private readonly IMailClient _mailClient;

        public EmailService(IMailClient mailClient, IConfiguration config)
        {
            _mailClient = mailClient;
            FirstMail = config["EmailAccounts:Email1"];
            SecondMail = config["EmailAccounts:Email2"];
        }

        /// <summary>
        /// Enqueues email for sending
        /// </summary>
        /// <param name="message">email to send</param>
        public void Send(MailMessage message)
        {
            _scheduledItems.Enqueue(message);
        }

        /// <summary>
        /// Creates mail message with provided data.
        /// </summary>
        /// <param name="receipient">to: email</param>
        /// <param name="subject">email subject</param>
        /// <param name="body">email content</param>
        /// <returns></returns>
        public MailMessage CreateEmail(string[] receipients, string subject, string body)
        {
            var mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(FirstMail);
            foreach (var item in receipients)
            {
                mailMessage.To.Add(item);
            }

            mailMessage.Body = body;
            mailMessage.Subject = subject;
            return mailMessage;
        }

        /// <summary>
        /// Task which runs periodically, checks the email queue if it's not empty sends first mail
        /// </summary>
        /// <param name="stoppingToken">Token used for stoping service</param>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                if (_scheduledItems.TryDequeue(out MailMessage message))
                {
                    await _mailClient.Send(message);
                }

                var delay = MillisecondsDelay + _rand.Next(MillisecondsDelay);
                await Task.Delay(delay, stoppingToken);
            }
        }

        /// <summary>
        /// Creates and send email informing that meeting request was created
        /// </summary>
        /// <param name="model">Created meeting request</param>
        public void SendRequestCreated(MeetingViewModel model)
        {
            var receipient = new[] { model.User.Email };
            var sb = new StringBuilder();

            sb.AppendFormat("Welcome, {0}", model.User.Username);
            sb.AppendLine();
            sb.AppendFormat("Your request to visit {0} was created.", model.Resource.Name);
            sb.AppendLine();
            sb.AppendLine("Please await response from moderation team.");
            sb.AppendFormat("Sincerly, {0} team", Consts.Brand);

            var body = sb.ToString();
            var subject = $"{Consts.Brand} – notice";

            var mailForUser = CreateEmail(receipient, subject, body);
            Send(mailForUser);


            receipient = new string[] { model.User.Email, SecondMail };
            subject = $"{model.User.Username} requested to visit {model.Resource.Name}";
            sb.Clear();
            sb.AppendFormat("There is a new request from user: {0}", model.User.Username);
            sb.AppendLine();
            sb.AppendFormat("To visit place: {0}", model.Resource.Name);
            sb.AppendLine();
            sb.AppendLine(Consts.Brand);
            body = sb.ToString();

            var mailForAdmin = CreateEmail(receipient, subject, body);
            Send(mailForAdmin);
        }

        /// <summary>
        /// Creates and send an email that meeting request has changed
        /// </summary>
        /// <param name="model">changed meeting request</param>
        public void SendRequestChanged(MeetingViewModel model)
        {
            var receipient = new[] { model.User.Email };
            var sb = new StringBuilder();

            var status = model.Approved ? "approved" : "canceled";

            sb.AppendFormat("Welcome,", model.User.Username);
            sb.AppendLine();
            sb.AppendLine("Your request to visit:");
            sb.AppendLine(model.Resource.Name);
            sb.AppendLine(model.Resource.Location);
            sb.Append("From: ");
            sb.AppendLine(model.DateStart.ToString());
            sb.Append("To: ");
            sb.AppendLine(model.DateEnd.ToString());
            sb.AppendFormat("was {0}.", status);
            sb.AppendLine();
            if (model.Approved)
            {
                sb.AppendFormat("Your entry code is: {0}", model.QrCode);
                sb.AppendLine();
                sb.AppendFormat("For more details please login to the {0} website", Consts.Brand);
                sb.AppendLine();
            }
            sb.AppendFormat("Sincerly, {0} team", Consts.Brand);

            var body = sb.ToString();
            var subject = $"{Consts.Brand} – Request {status}";

            var mailForUser = CreateEmail(receipient, subject, body);
            Send(mailForUser);
        }

        /// <summary>
        /// Creates and sends an email that user role was changed
        /// </summary>
        /// <param name="model"></param>
        public void SendUserRoleChanged(UserViewModel model)
        {
            var receipient = new[] { model.Email };
            var sb = new StringBuilder();

            sb.AppendFormat("Welcome, {0}", model.Username);
            sb.AppendLine();
            if (model.Role == UserRole.NewUser)
            {
                sb.AppendFormat("Your request to join {0} was accepted.", Consts.Brand);
                sb.AppendLine();
            }
            sb.AppendFormat("Your are now a {0} of the service.", model.Role);
            sb.AppendLine();
            sb.AppendFormat("Sincerly, {0} team", Consts.Brand);

            var body = sb.ToString();
            var subject = $"{Consts.Brand} – Welcome onboard!";

            var mailForUser = CreateEmail(receipient, subject, body);
            Send(mailForUser);
        }

    }
}