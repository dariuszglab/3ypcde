﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SimplyLogin1.Data.Entities;

namespace SimplyLogin1.Data
{
    /// <summary>
    /// Represents database context
    /// </summary>
    public class DbCtx : IdentityDbContext<User, Role, string>
    {
        public DbCtx() { }

        public DbCtx(DbContextOptions<DbCtx> options) : base(options)
        {
        }

        public DbSet<Resource> Resources { get; set; }
        public DbSet<Meeting> Meetings { get; set; }
        //public DbSet<EmailTask> EmilTasks { get; set; }
        //public DbSet<Message> Messages { get; set; }
    }
}
