using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using SimplyLogin1.Common;
using SimplyLogin1.Data;
using SimplyLogin1.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimplyLogin1.Data
{
    /// <summary>
    /// Inserts some test data to the database at the start of application.
    /// </summary>
    public class DbSeeder
    {
        private readonly DbCtx _ctx;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> roleManager;
        private readonly IConfiguration _config;

        public DbSeeder(DbCtx context,
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            IConfiguration configuration)
        {
            _ctx = context;
            _userManager = userManager;
            this.roleManager = roleManager;
            _config = configuration;
        }

        public async Task Seed()
        {
            await _ctx.Database.EnsureCreatedAsync();

            await SetupRoles();
            await CreateData();

            await _ctx.SaveChangesAsync();
        }

        private async Task CreateData()
        {
            var admin = new
            {
                UserName = _config["Users:Admin:Username"],
                Email = _config["Users:Admin:Email"],
                Role = _config["Users:Admin:Role"],
                Password = _config["Users:Admin:Password"],
            };
            var moder = new
            {
                UserName = _config["Users:Moder:Username"],
                Email = _config["Users:Moder:Email"],
                Role = _config["Users:Moder:Role"],
                Password = _config["Users:Moder:Password"],
            };
            var member = new
            {
                UserName = _config["Users:Member:Username"],
                Email = _config["Users:Member:Email"],
                Role = _config["Users:Member:Role"],
                Password = _config["Users:Member:Password"],
            };
            var newuser = new
            {
                UserName = _config["Users:NewUser:Username"],
                Email = _config["Users:NewUser:Email"],
                Role = _config["Users:NewUser:Role"],
                Password = _config["Users:NewUser:Password"],
            };
            var realUser = new
            {
                UserName = _config["Users:Jhon:Username"],
                Email = _config["Users:Jhon:Email"],
                Role = _config["Users:Jhon:Role"],
                Password = _config["Users:Jhon:Password"],
            };

            var users = new[] { admin, moder, member, newuser, realUser };
            var usersList = new List<User>();
            foreach (var item in users)
            {
                var dbUser = await _userManager.FindByNameAsync(item.UserName);
                if (dbUser == null)
                {
                    dbUser = new User() { UserName = item.UserName, Email = item.Email };
                    await _userManager.CreateAsync(dbUser, item.Password);
                    await _userManager.AddToRoleAsync(dbUser, item.Role);
                };
                usersList.Add(dbUser);
            }


            var place1 = new Resource()
            {
                Name = "IT Team",
                Location = "1 Dame Ct, Dublin 2, Ireland"
            };
            var place2 = new Resource()
            {
                Name = "Finance Team",
                Location = "51 South Great George's Street, Dublin 2, Ireland"
            };
            var place3 = new Resource()
            {
                Name = "Marketing Team",
                Location = "15 Merrion Row, Dublin, Ireland"
            };
            var resources = new Resource[] { place1, place2, place3 };
            foreach (var res in resources)
            {
                var exists = _ctx.Resources
                    .Where(x => x.Name == res.Name)
                    .FirstOrDefault();
                if (exists == null)
                {
                    await _ctx.Resources.AddAsync(res);
                }
            }


            var now = DateTime.Now;
            var meeting1 = new Meeting()
            {
                User = usersList[0],
                Resource = place1,
            };
            var meeting2 = new Meeting()
            {
                User = usersList[1],
                Resource = place2,
                DateStart = now.AddDays(1),
                DateEnd = now.AddDays(2),
                Approved = true,
                QrCode = Guid.NewGuid().ToString()
            };
            var meeting3 = new Meeting()
            {
                User = usersList[2],
                Resource = place3
            };
            var meeting4 = new Meeting()
            {
                User = usersList[3],
                Resource = place1,
                DateStart = now,
                DateEnd = now.AddDays(10),
                Approved = true,
                QrCode = Guid.NewGuid().ToString()
            };

            var meetings = new Meeting[] { meeting1, meeting2, meeting3, meeting4 };
            foreach (var mee in meetings)
            {
                var exists = _ctx.Meetings
                    .Where(x => x.Resource.Name == mee.Resource.Name)
                    .FirstOrDefault();
                if (exists == null)
                {
                    await _ctx.Meetings.AddAsync(mee);
                }
            }
        }

        private async Task SetupRoles()
        {
            foreach (var role in UserRole.All)
            {
                var exists = await roleManager.RoleExistsAsync(role);
                if (exists) continue;
                var res = await roleManager.CreateAsync(new Role(role));
            }
        }
    }
}