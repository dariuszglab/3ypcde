﻿using Microsoft.EntityFrameworkCore;
using SimplyLogin1.Data.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace SimplyLogin1.Data
{
    /// <summary>
    /// Base class with generic functionality for repositories
    /// </summary>
    public abstract class Repository<TEntity> where TEntity : class, IEntity
    {
        private readonly DbCtx _context;

        public Repository(DbCtx context)
        {
            _context = context;
        }

        protected IQueryable<TEntity> GetAll()
        {
            return _context.Set<TEntity>().AsNoTracking();
        }

        protected async Task<TEntity> GetById(string id)
        {
            return await _context.Set<TEntity>()
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        protected async Task Create(TEntity entity)
        {
            await _context.Set<TEntity>().AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        protected async Task<bool> Update(TEntity entity)
        {
            try
            {
                _context.Set<TEntity>().Update(entity);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }
        }

        protected async Task Delete(string id)
        {
            var entity = await GetById(id);
            _context.Set<TEntity>().Remove(entity);
            await _context.SaveChangesAsync();
        }
    }
}
