﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SimplyLogin1.Data.Entities;
using SimplyLogin1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimplyLogin1.Data
{
    /// <summary>
    /// Handles access to data in DB for Meetings
    /// </summary>
    public class MeetingRepository : Repository<Meeting>
    {
        private readonly DbCtx _ctx;
        private readonly IMapper _mapper;

        public MeetingRepository(DbCtx context, IMapper mapper) : base(context)
        {
            _ctx = context;
            _mapper = mapper;
        }

        public async Task Make(MeetingViewModel model)
        {
            var item = _mapper.Map<MeetingViewModel, Meeting>(model);
            item.Id = null;
            _ctx.Entry(item.Resource).State = EntityState.Unchanged;
            await Create(item);
        }

        public async Task<IEnumerable<MeetingViewModel>> All()
        {
            var all = await base.GetAll()
               .Include(x => x.User)
               .Include(x => x.Resource)
               .Select(x => _mapper.Map<Meeting, MeetingViewModel>(x))
               .ToListAsync();
            return all;
        }

        public async Task<MeetingViewModel> Find(string id)
        {
            if (id == null)
            {
                return null;
            }
            var found = await _ctx.Meetings
                .Where(x => x.Id == id)
                .Include(x => x.Resource)
                .Include(x => x.User)
                .FirstOrDefaultAsync();

            return _mapper.Map<Meeting, MeetingViewModel>(found);
        }

        public async Task<bool> Edit(MeetingViewModel model)
        {
            var item = _mapper.Map<MeetingViewModel, Meeting>(model);
            return await Update(item);
        }

        public async Task Remove(string id)
        {
            await base.Delete(id);
        }

        public async Task<string> ChangeApprove(MeetingViewModel model)
        {
            var item = await _ctx.Meetings
                .FirstOrDefaultAsync(x => x.Id == model.Id);

            if (model.Approved)
            {
                item.Approved = true;
                item.QrCode = Guid.NewGuid().ToString();
            }
            else
            {
                item.Approved = false;
                item.QrCode = string.Empty;
            }

            var res = await Update(item);
            if (res)
            {
                return item.QrCode;
            }
            return string.Empty;
        }

        public async Task<IEnumerable<MeetingViewModel>> All(User user)
        {
            var all = await base.GetAll()
               .Include(x => x.User)
               .Where(x => x.User.Id == user.Id)
               .Include(x => x.Resource)
               .Select(x => _mapper.Map<Meeting, MeetingViewModel>(x))
               .ToListAsync();
            return all;
        }
    }
}
