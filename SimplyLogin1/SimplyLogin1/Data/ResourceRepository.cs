﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SimplyLogin1.Data.Entities;
using SimplyLogin1.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimplyLogin1.Data
{
    /// <summary>
    /// Handles access to data in DB for Resources
    /// </summary>
    public class ResourceRepository : Repository<Resource>
    {
        private readonly IMapper _mapper;
        private readonly DbCtx _context;

        public ResourceRepository(DbCtx context, IMapper mapper) : base(context)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task Make(ResourceViewModel model)
        {
            var item = _mapper.Map<ResourceViewModel, Resource>(model);
            await Create(item);
        }

        public async Task<IEnumerable<ResourceViewModel>> All()
        {
            return await GetAll()
                .Select(x => _mapper.Map<Resource, ResourceViewModel>(x))
                .ToListAsync();
        }

        public async Task<ResourceViewModel> Find(string id)
        {
            if (id == null)
            {
                return null;
            }

            var item = await GetById(id);
            return _mapper.Map<Resource, ResourceViewModel>(item);
        }

        public async Task<bool> Edit(ResourceViewModel model)
        {
            var item = _mapper.Map<ResourceViewModel, Resource>(model);
            return await Update(item);
        }

        public async Task Remove(string id)
        {
            if (id == null)
            {
                return;
            }

            var meetingsToDelete = await _context.Meetings.
                Where(x => x.Resource.Id == id)
                .ToListAsync();

            _context.Meetings.RemoveRange(meetingsToDelete);

            await base.Delete(id);
        }

        public async Task<IEnumerable<ResourceViewModel>> Search(string searchPhrase)
        {
            searchPhrase = searchPhrase.Trim();

            var items = await _context.Resources
                .Where(x => x.Name.Contains(searchPhrase) || x.Location.Contains(searchPhrase))
                .Select(x => _mapper.Map<Resource, ResourceViewModel>(x))
                .ToListAsync();

            return items;
        }
    }
}
