﻿using Microsoft.AspNetCore.Identity;

namespace SimplyLogin1.Data.Entities
{
    public class Role : IdentityRole, IEntity
    {
        public Role()
        {
        }

        public Role(string roleName) : base(roleName)
        {
        }
    }
}
