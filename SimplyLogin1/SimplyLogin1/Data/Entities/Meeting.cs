﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimplyLogin1.Data.Entities
{
    public class Meeting : IEntity
    {
        public string Id { get; set; }
        [DisplayName("Location")]
        public Resource Resource { get; set; }
        [DisplayName("Participant")]
        public User User { get; set; }
        public bool Approved { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime DateStart { get; set; } = DateTime.Now;
        [Column(TypeName = "datetime2")]
        public DateTime DateEnd { get; set; } = DateTime.Now;
        public string QrCode { get; set; }
    }
}
