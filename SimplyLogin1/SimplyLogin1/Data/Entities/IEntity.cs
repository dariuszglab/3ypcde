﻿namespace SimplyLogin1.Data.Entities
{
    public interface IEntity
    {
        string Id { get; }
    }
}
