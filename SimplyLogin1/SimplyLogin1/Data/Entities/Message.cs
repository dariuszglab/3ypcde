﻿namespace SimplyLogin1.Data.Entities
{
    public class Message : IEntity
    {
        public string Id { get; }
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
