﻿namespace SimplyLogin1.Data.Entities
{
    public class Resource : IEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
    }
}
