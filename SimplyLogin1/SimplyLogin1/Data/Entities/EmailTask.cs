﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimplyLogin1.Data.Entities
{
    public class EmailTask : IEntity
    {
        public string Id { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime Date { get; set; }
        public Message Message { get; set; }
        public bool Sent { get; set; }
    }
}
