﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SimplyLogin1.Common;
using SimplyLogin1.Data;
using SimplyLogin1.Data.Entities;
using SimplyLogin1.Models;
using SimplyLogin1.Services;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SimplyLogin1.Controllers
{
    /// <summary>
    /// Handles main views and functionality of the application.
    /// </summary>
    public class HomeController : BaseController
    {
        private readonly ResourceRepository _resourceRepository;
        private readonly MeetingRepository _meetingRepository;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly EmailService _emailService;
        private readonly ILogger<HomeController> _logger;

        public HomeController(
            ResourceRepository resourceRepository,
            MeetingRepository meetingRepository,
            IUserService userService,
            IMapper mapper, EmailService emailService,
            ILogger<HomeController> logger)
        {
            _resourceRepository = resourceRepository;
            _meetingRepository = meetingRepository;
            _userService = userService;
            _mapper = mapper;
            _emailService = emailService;
            _logger = logger;
        }

        /// <summary>
        /// Goes to main page
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            return await Index(string.Empty);
        }

        /// <summary>
        /// Goes to main index page of the app and returnts meetings and locations according to user role and search phrase
        /// </summary>
        /// <param name="searchPhrase">pharse to search locations with</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Index(string searchPhrase)
        {
            var vm = new HomeViewModel() { SearchPhrase = searchPhrase };
            if (IsAuth)
            {
                if (searchPhrase.IsEmpty())
                {
                    vm.Resources = await _resourceRepository.All();
                }
                else
                {
                    vm.Resources = await _resourceRepository.Search(searchPhrase);
                }

                if (User.IsInRole(UserRole.Member))
                {
                    var user = await _userService.GetCurrentUser(User);
                    vm.Meetings = await _meetingRepository.All(user);
                }
                else if (!User.IsInRole(UserRole.NewUser))
                {
                    vm.Meetings = await _meetingRepository.All();
                }
            }
            return View(vm);
        }

        /// <summary>
        /// Goes to about page
        /// </summary>
        /// <returns></returns>
        [HttpGet(Consts.About)]
        public IActionResult About()
        {
            return View();
        }

        /// <summary>
        /// Goes to contact page
        /// </summary>
        /// <returns></returns>
        [HttpGet(Consts.Contact)]
        public IActionResult Contact()
        {
            return View();
        }

        /// <summary>
        /// Saves contact message
        /// </summary>
        /// <param name="model">Contact form</param>
        /// <returns></returns>
        [HttpPost(Consts.Contact)]
        public IActionResult Contact(ContactViewModel model)
        {
            var message = _mapper.Map<ContactViewModel, Message>(model);
            //save to the database, not implemented
            ViewData["Message"] = "Your message was sent.";
            return View();
        }

        /// <summary>
        /// Goes to error view
        /// </summary>
        /// <returns></returns>
        public IActionResult Error()
        {
            return View(new ErrorViewModel
            {
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier
            });
        }

        /// <summary>
        /// Goes to admin panel view
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = UserRole.Moderation)]
        public IActionResult AdminPanel()
        {
            return View();
        }

        /// <summary>
        /// Goes to view for date select for meeting request
        /// </summary>
        /// <param name="id">location id</param>
        /// <returns></returns>
        [Authorize(Roles = UserRole.Memberhsip)]
        [HttpGet]
        public async Task<IActionResult> DateSelect(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var resource = await _resourceRepository.Find(id);
            var vm = new MeetingViewModel
            {
                Resource = resource
            };
            TempData["resourceId"] = id;
            return View(vm);
        }

        /// <summary>
        /// Selects dates range for meeting request
        /// </summary>
        /// <param name="model">meeting request form</param>
        /// <returns></returns>
        [Authorize(Roles = UserRole.Memberhsip)]
        [HttpPost]
        public async Task<IActionResult> DateSelect(MeetingViewModel model)
        {
            var resourceId = TempData["resourceId"] as string;
            var resource = await _resourceRepository.Find(resourceId);
            var user = await _userService.GetCurrentUser(User);
            var userVm = _mapper.Map<User, UserViewModel>(user);
            model.Resource = resource;
            model.User = userVm;

            await _meetingRepository.Make(model);

            _emailService.SendRequestCreated(model);

            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Accepts meeting
        /// </summary>
        /// <param name="id">meeting id</param>
        /// <returns></returns>
        [Authorize(Roles = UserRole.Memberhsip)]
        public async Task<IActionResult> Approve(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vm = await _meetingRepository.Find(id);
            vm.Approved = true;
            var result = await _meetingRepository.ChangeApprove(vm);

            vm.QrCode = result;
            _emailService.SendRequestChanged(vm);
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Cancels a meeting 
        /// </summary>
        /// <param name="model">meeting id</param>
        /// <returns></returns>
        [Authorize(Roles = UserRole.Memberhsip)]
        public async Task<IActionResult> Cancel(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vm = await _meetingRepository.Find(id);
            vm.Approved = false;
            await _meetingRepository.ChangeApprove(vm);
            _emailService.SendRequestChanged(vm);
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Goes to meeting details view
        /// </summary>
        /// <param name="id">meeting id</param>
        [Authorize(Roles = UserRole.Memberhsip)]
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var viewModel = await _meetingRepository.Find(id);

            if (viewModel == null)
            {
                return NotFound();
            }

            return View(viewModel);
        }

        /// <summary>
        /// Deletes the meeting
        /// </summary>
        /// <param name="id"> meeting id</param>
        /// <returns></returns>
        [Authorize(Roles = UserRole.Admin)]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            await _meetingRepository.Remove(id);
            return RedirectToAction(nameof(Index));

        }
    }
}
