﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using SimplyLogin1.Common;

namespace SimplyLogin1.Controllers
{
    /// <summary>
    /// Base class for controllers, contains few helper methods.
    /// </summary>
    public abstract class BaseController : Controller
    {
        public bool IsAuth { get { return this.User.Identity.IsAuthenticated; } }
        public bool IsValid { get { return this.ModelState.IsValid; } }

        /// <summary>
        /// Extracts return url from the request
        /// </summary>
        /// <param name="query">Part of request</param>
        /// <param name="result">return url</param>
        /// <returns>true if succeded</returns>
        public bool GetReturnUrl(IQueryCollection query, out string result)
        {
            result = string.Empty;
            var found = query.TryGetValue("ReturnUrl", out StringValues returnUrls);
            if (found)
            {
                result = returnUrls.FirstOrDefault();
            }

            return result.IsNotEmpty();
        }
    }
}
