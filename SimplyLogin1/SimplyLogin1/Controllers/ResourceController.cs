﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SimplyLogin1.Common;
using SimplyLogin1.Data;
using SimplyLogin1.Models;
using System.Threading.Tasks;

namespace SimplyLogin1.Controllers
{        
    /// <summary>
    /// Handles all actions related to resource (locations) management.
    /// </summary>
    [Authorize(Roles = UserRole.Moderation)]
    public class ResourceController : BaseController
    {
        private readonly ResourceRepository _repo;
        private readonly ILogger<ResourceController> _logger;

        public ResourceController(ResourceRepository resourceRepository, 
            ILogger<ResourceController> logger)
        {
            _repo = resourceRepository;
            _logger = logger;
        }

        /// <summary>
        /// Goes to main resource page showing list of all resources
        /// </summary>
        public async Task<IActionResult> Index()
        {
            var resources = await _repo.All();
            return View(resources);
        }

        /// <summary>
        /// Goes to resource details page
        /// </summary>
        /// <param name="id">resource id</param>
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var viewModel = await _repo.Find(id);
            if (viewModel == null)
            {
                return NotFound();
            }

            return View(viewModel);
        }

        /// <summary>
        /// Goes to resource creation view
        /// </summary>
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Creates new resource
        /// </summary>
        /// <param name="viewModel">resource form data</param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Location")] ResourceViewModel viewModel)
        {
            if (IsValid)
            {
                await _repo.Make(viewModel);
                return RedirectToAction(nameof(Index));
            }
            return View(viewModel);
        }

        /// <summary>
        /// Goes to edit view
        /// </summary>
        /// <param name="id">resource id</param>
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var viewModel = await _repo.Find(id);
            if (viewModel == null)
            {
                return NotFound();
            }

            return View(viewModel);
        }

        /// <summary>
        /// Saves edited changes
        /// </summary>
        /// <param name="id">resource id</param>
        /// <param name="viewModel">Resource form data</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Name,Location")] ResourceViewModel viewModel)
        {
            if (id != viewModel.Id)
            {
                return NotFound();
            }

            if (IsValid)
            {
                await _repo.Edit(viewModel);
                return RedirectToAction(nameof(Index));
            }
            return View(viewModel);
        }

        /// <summary>
        /// Goes to delete confirmation page 
        /// </summary>
        /// <param name="id">resource id</param>
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var viewModel = await _repo.Find(id);
            if (viewModel == null)
            {
                return NotFound();
            }

            return View(viewModel);
        }
        
        /// <summary>
        /// Deletes selected resource
        /// </summary>
        /// <param name="id">resource id</param>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            await _repo.Remove(id);
            return RedirectToAction(nameof(Index));
        }

    }
}
