using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SimplyLogin1.Common;
using SimplyLogin1.Models;
using SimplyLogin1.Services;
using System.Threading.Tasks;

namespace SimplyLogin1.Controllers
{
    /// <summary>
    /// Handles tasks related to users, accounts, login, registration etc.
    /// </summary>
    public class AccountController : BaseController
    {
        private const string Index = nameof(HomeController.Index);
        private const string Account = Consts.Account;
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Goes to login view
        /// </summary>
        /// <returns></returns>
        public IActionResult Login()
        {
            if (IsAuth)
            {
                return RedirectToAction(Index, Consts.Home);
            }
            return View();
        }

        /// <summary>
        /// Logs in the user 
        /// </summary>
        /// <param name="model">Filled login form</param>
        /// <returns>Goes to the page that required login or to home on succcess</returns>
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (IsValid)
            {
                var result = await _userService.Login(model);
                if (result)
                {
                    if (GetReturnUrl(Request.Query, out string url))
                    {
                        return Redirect(url);
                    }
                    else
                    {
                        return RedirectToAction(Index, Consts.Home);
                    }
                }
            }

            ModelState.AddModelError("", "Login failure");

            return View();
        }

        /// <summary>
        /// Goes to registration page if user is not logged in
        /// </summary>
        /// <returns></returns>
        public IActionResult Register()
        {
            if (IsAuth)
            {
                return RedirectToAction(Index, Consts.Home);
            }
            return View();
        }

        /// <summary>
        /// Registers the user into the website
        /// </summary>
        /// <param name="model">Filled in registration form</param>
        /// <returns>Goes to home page on success</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (IsValid)
            {
                var result = await _userService.Register(model);
                if (result)
                {
                    await _userService.Login(model);
                    return RedirectToAction(Index, Consts.Home);
                }
                ModelState.AddModelError("", "Registration failure");
            }

            return View(model);
        }

        /// <summary>
        /// Logs out currently logged in user
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            if (IsAuth)
            {
                await _userService.Logout();
                return RedirectToAction(Index, Consts.Home);
            }
            return View();
        }

        /// <summary>
        /// Goes to view with a list of all users
        /// </summary>
        [Authorize(Roles = UserRole.Moderation)]
        [HttpGet]
        public async Task<IActionResult> UserList()
        {
            var users = await _userService.GetAllUsers();
            return View(users);
        }

        /// <summary>
        /// Deletes user from database
        /// </summary>
        /// <param name="id">user id</param>
        [Authorize(Roles = UserRole.Moderation)]
        [HttpGet]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var success = await _userService.Delete(id);
            if (success)
            {
                return RedirectToAction(nameof(UserList), Account);
            }

            ModelState.AddModelError("", "Deletion failure");
            return View();
        }

        /// <summary>
        /// Sets user as a Member
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns>Goes to user list on success</returns>
        [Authorize(Roles = UserRole.Moderation)]
        [HttpGet]
        public async Task<IActionResult> AcceptMember(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var success = await _userService.SetRole(id, UserRole.Member);
            if (success)
            {
                return RedirectToAction(nameof(UserList), Account);
            }

            ModelState.AddModelError("", "Setting role failure");
            return RedirectToAction(nameof(UserList), Account);
        }

        /// <summary>
        /// Sets user as a moderator
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns>Goes to user list on success</returns>
        [Authorize(Roles = UserRole.Admin)]
        [HttpGet]
        public async Task<IActionResult> AcceptModerator(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var success = await _userService.SetRole(id, UserRole.Moderator);
            if (success)
            {
                return RedirectToAction(nameof(UserList), Account);
            }

            ModelState.AddModelError("", "Setting role failure");
            return RedirectToAction(nameof(UserList), Account);
        }

        /// <summary>
        /// Goes to user details view
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = UserRole.Memberhsip)]
        [HttpGet]
        public async Task<IActionResult> Details()
        {
            var user = await _userService.GetCurrentUser(User);
            var model = _userService.Map(user);
            return View(model);
        }

        /// <summary>
        /// Goes to change password view
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = UserRole.Memberhsip)]
        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        /// <summary>
        /// Changes password of the user whos filling the form
        /// </summary>
        /// <param name="model">Password changing form</param>
        /// <returns>Goes to details if succeded</returns>
        [Authorize(Roles = UserRole.Memberhsip)]
        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (IsValid)
            {
                var user = await _userService.GetCurrentUser(User);
                var res = await _userService.ChangePassword(user, model.CurrentPassword, model.NewPassword);
                if (res)
                {
                   return RedirectToAction(nameof(Details), Consts.Account);
                }
            }

            ModelState.AddModelError("", "Change failed");
            return View();
        }


        /// <summary>
        /// Shows accesd denied page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }
    }
}