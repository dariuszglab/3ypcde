﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using SimplyLogin1.Common;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using ZXing;
using ZXing.Common;

namespace SimplyLogin1.Views
{
    /// <summary>
    /// Class represents custom tag used to display QR code
    /// </summary>
    [HtmlTargetElement("qrcode", TagStructure = TagStructure.WithoutEndTag)]
    public class QrCodeTagHelper : TagHelper
    {
        /// <summary>
        /// Interprets data from input properties to render QR tag image
        /// </summary>
        /// <param name="context">tag properties</param>
        /// <param name="output">output tag</param>
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var valueAttr = context.AllAttributes["value"].Value.ToString();
            var widthAttr = context.AllAttributes["width"].Value.ToString();
            var heightAttr = context.AllAttributes["height"].Value.ToString();

            var width = widthAttr.Parse();
            var height = heightAttr.Parse();

            var barcodeWriter = new BarcodeWriterPixelData();
            barcodeWriter.Format = BarcodeFormat.QR_CODE;
            var options = new EncodingOptions
            {
                Height = height,
                Width = width,
                Margin = 0
            };
            barcodeWriter.Options = options;

            var data = barcodeWriter.Write(valueAttr);

            using (var bitmap = new Bitmap(data.Width, data.Height, PixelFormat.Format32bppRgb))
            using (var ms = new MemoryStream())
            {
                var rectangle = new Rectangle(0, 0, data.Width, data.Height);
                var bitmapData = bitmap.LockBits(rectangle, ImageLockMode.WriteOnly, PixelFormat.Format32bppRgb);
                try
                {
                    System.Runtime.InteropServices.Marshal.Copy(data.Pixels, 0, bitmapData.Scan0, data.Pixels.Length);
                }
                finally
                {
                    bitmap.UnlockBits(bitmapData);
                }

                bitmap.Save(ms, ImageFormat.Png);
                output.TagName = "img";
                output.Attributes.Clear();
                output.Attributes.Add("width", width);
                output.Attributes.Add("height", height);
                var arrayAsString = Convert.ToBase64String(ms.ToArray());
                var src = string.Format("data:image/png;base64,{0}", arrayAsString);
                output.Attributes.Add("src", src);
            }
        }

    }
}
