using SimplyLogin1.Common;

namespace SimplyLogin1.Models
{
    /// <summary>
    /// Form for errors
    /// </summary>
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => RequestId.IsNotEmpty();
    }
}