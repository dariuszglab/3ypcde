﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimplyLogin1.Models
{
    /// <summary>
    /// Form for generic meeting operations
    /// </summary>
    public class MeetingViewModel
    {
        public string Id { get; set; }
        public ResourceViewModel Resource { get; set; }
        public UserViewModel User { get; set; }
        public bool Approved { get; set; }
        [Required]
        public DateTime DateStart { get; set; } = DateTime.Now;
        [Required]
        public DateTime DateEnd { get; set; } = DateTime.Now.AddHours(2);
        public string QrCode { get; set; }

        [NotMapped]
        public string ApprovedString
        {
            get
            {
                return Approved ? "Yes" : "No";
            }
        }
    }
}
