using System.ComponentModel.DataAnnotations;

namespace SimplyLogin1.Models
{
    /// <summary>
    /// Form for password change
    /// </summary>
    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Confirm password doesn't match, Type again !")]
        public string NewPasswordConfirmed { get; set; }
    }
}
