﻿using System.ComponentModel.DataAnnotations;

namespace SimplyLogin1.Models
{    
    /// <summary>
     /// Form for generic resource operations
     /// </summary>
    public class ResourceViewModel
    {
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Location { get; set; }
    }
}
