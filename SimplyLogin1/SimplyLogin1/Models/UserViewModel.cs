﻿using System.ComponentModel.DataAnnotations;

namespace SimplyLogin1.Models
{
    /// <summary>
    /// Form for generic user operations
    /// </summary>
    public class UserViewModel
    {
        public string Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Role { get; set; }
    }
}
