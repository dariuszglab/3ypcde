using System.ComponentModel.DataAnnotations;

namespace SimplyLogin1.Models
{
    /// <summary>
    /// Form for registering user
    /// </summary>
    public class RegisterViewModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
        [Range(typeof(bool), "true", "true", ErrorMessage = "Yo have to agree to Terms and Conditions!")]
        public bool AcceptTerms { get; set; }

        public string PhoneNumber { get; set; }
    }
}