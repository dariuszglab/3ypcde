﻿using System.Collections.Generic;
using System.Linq;

namespace SimplyLogin1.Models
{
    /// <summary>
    /// Form for generic main view operations
    /// </summary>
    public class HomeViewModel
    {
        public IEnumerable<ResourceViewModel> Resources { get; set; } = Enumerable.Empty<ResourceViewModel>();
        public IEnumerable<MeetingViewModel> Meetings { get; set; } = Enumerable.Empty<MeetingViewModel>();
        public string SearchPhrase { get; set; } = string.Empty;
    }
}
