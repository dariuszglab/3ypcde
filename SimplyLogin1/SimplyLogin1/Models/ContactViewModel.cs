using System.ComponentModel.DataAnnotations;

namespace SimplyLogin1.Models
{
    /// <summary>
    /// Form for contact message
    /// </summary>
    public class ContactViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Body { get; set; }
    }
}
