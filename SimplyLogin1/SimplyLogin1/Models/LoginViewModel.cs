﻿using System.ComponentModel.DataAnnotations;

namespace SimplyLogin1.Models
{
    /// <summary>
    /// Form for logging in user
    /// </summary>
    public class LoginViewModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool RememberMe { get; set; }

    }
}
