﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace SimplyLogin1
{
    /// <summary>
    /// Program starting class
    /// </summary>
    public sealed class Program
    {
        /// <summary>
        /// Program etry point
        /// </summary>
        public static void Main(string[] args)
        {
            //this disables annoying ApplicationInsights crap in output
            Microsoft.ApplicationInsights.Extensibility.Implementation.
                TelemetryDebugWriter.IsTracingDisabled = true;

            BuildWebHost(args).Run();
        }

        /// <summary>
        /// Configures the app to handle http requests
        /// </summary>
        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost
                .CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(SetupConfiguration)
                .UseIISIntegration()// this has to be disabled for unit tests to work properly
                .UseStartup<Startup>()
                .Build();
        }

        /// <summary>
        /// Setups reading configuration from config files
        /// </summary>
        private static void SetupConfiguration(WebHostBuilderContext context, IConfigurationBuilder builder)
        {
            //remove default stuff
            builder.Sources.Clear();
            builder.AddJsonFile("config.json", false, true);
        }
    }
}
