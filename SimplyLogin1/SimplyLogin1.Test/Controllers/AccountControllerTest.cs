using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using SimplyLogin1.Controllers;
using SimplyLogin1.Data.Entities;
using SimplyLogin1.Models;
using SimplyLogin1.Services;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SimplyLogin1.Test.Controllers
{
    [TestFixture]
    public class AccountControllerTest
    {
        [Test]
        public void Lifcycle()
        {
            var service = Create();
            service.Dispose();
            Assert.Pass();
        }

        [Test]
        public void ChangePasswordRedirectsToDetails()
        {
            var userService = new Mock<IUserService>();
            userService
                .Setup(x => x.ChangePassword(It.IsAny<User>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            userService
                .Setup(x => x.GetCurrentUser(It.IsAny<ClaimsPrincipal>()))
                .Returns(Task.FromResult(new User()));

            var service = Create(userService.Object);
            var model = new ChangePasswordViewModel()
            {
                CurrentPassword = "a",
                NewPassword = "b"
            };
            var result = service.ChangePassword(model).Result;
            var casted = result as RedirectToActionResult;

            Assert.IsTrue(casted != null);
            Assert.IsTrue(casted.ControllerName == "Account");
            Assert.IsTrue(casted.ActionName == "Details");
        }

        public static AccountController Create(IUserService userService = null)
        {
            userService = userService ?? new Mock<IUserService>().Object;
            return new AccountController(userService);
        }
    }
}
