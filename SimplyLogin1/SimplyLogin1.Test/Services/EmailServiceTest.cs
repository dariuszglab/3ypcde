﻿using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using SimplyLogin1.Models;
using SimplyLogin1.Services;
using System;

namespace SimplyLogin1.Test.Services
{
    [TestFixture]
    public class EmailServiceTest
    {
        [Test]
        public void Lifcycle()
        {
            var service = Create();
            service.Dispose();
            Assert.Pass();
        }

        [Test]
        public void TestGeneratedMails()
        {
            var service = Create();

            var meeting = CreateMeetingVM();
            service.SendRequestChanged(meeting);
            service.SendRequestCreated(meeting);

            var userVm = CreateUserVM();
            service.SendUserRoleChanged(userVm);

            Assert.Pass();
        }

        public static EmailService Create()
        {
            var mailClient = new ConsoleMailClient();
            var config = new Mock<IConfiguration>();
            config
                .Setup(x => x[It.IsAny<string>()])
                .Returns("some@em.ail");

            return new EmailService(mailClient, config.Object);
        }

        private MeetingViewModel CreateMeetingVM()
        {
            return new MeetingViewModel()
            {
                User = CreateUserVM(),
                Resource = CreateResourceVM(),
                DateStart = DateTime.Now,
                DateEnd = DateTime.Now.AddDays(1),
                Approved = true,
                QrCode = "QrCode"
            };
        }

        private UserViewModel CreateUserVM()
        {
            return new UserViewModel()
            {
                Email = "some@em.ail",
                Username = "Username",
                Role = "Role"
            };
        }

        private ResourceViewModel CreateResourceVM()
        {
            return new ResourceViewModel()
            {
                Name = "Name",
                Location = "Location",
            };
        }

    }
}
